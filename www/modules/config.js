(function(angular) {
  'use strict';

  angular.module('app')

    .constant('appConfig', {
      providers: {
        vk: {
          authURL: 'https://oauth.vk.com/authorize?client_id=5986515&scope=photos,offline&redirect_uri=https://oauth.vk.com/blank.html&display=touch&response_type=token',
          client_id: '5986515',
          redirect_uri: 'https://oauth.vk.com/blank.html'
        },
        mailru: {
          authURL: 'https://connect.mail.ru/oauth/authorize?client_id=753423&response_type=token&redirect_uri=https://connect.mail.ru/oauth/success.html',
          client_id: '753423',
          private_key: '943d7f37626bd91ab601d5d607063097',
          redirect_uri: 'https://connect.mail.ru/oauth/success.html'
        }
      }
    });

})(angular);
