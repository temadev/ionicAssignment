(function(angular) {
  'use strict';

  angular.module('app', ['ionic', 'ngIOS9UIWebViewPatch', 'app.main', 'app.auth'])
    .run(appRun)
    .config(appConfig);

  appConfig.$inject = ['$urlRouterProvider', '$ionicConfigProvider'];
  function appConfig($urlRouterProvider, $ionicConfigProvider) {
    $ionicConfigProvider.navBar.alignTitle('center');
    $urlRouterProvider.otherwise('/login');
  }

  appRun.$inject = ['$ionicPlatform'];
  function appRun($ionicPlatform) {
    $ionicPlatform.ready(function() {
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);
      }
    });
  }

})(angular);
