(function(angular) {
  'use strict';

  angular.module('app.main')
    .service('userProfile', userProfile);

  userProfile.$inject = ['$q', '$http', 'appConfig', 'md5'];
  function userProfile($q, $http, appConfig, md5) {
    this.getData = function() {
      var access_token = localStorage.getItem('access_token');
      var provider = localStorage.getItem('provider');
      var uid = localStorage.getItem('uid');
      var profileData;

      if (!access_token || !provider) {
        profileData = profileErr();
      } else {
        switch (provider) {
          case 'vk':
            profileData = vkProfile(access_token);
            break;
          case 'mailru':
            profileData = uid ? mailruProfile(access_token, uid) : profileErr();
            break;
          default:
            profileData = profileErr();
        }
      }

      return profileData;
    };

    function vkProfile(access_token) {
      var url = 'https://api.vk.com/method/account.getProfileInfo?access_token=' + access_token + '&v=V';
      return getProfile(url)
        .then(function(response) {
          var profile = {};
          if (response.data) {
            profile.first_name = response.data.response.first_name;
            profile.last_name = response.data.response.last_name;
            profile.full_name = profile.first_name + ' ' + profile.last_name;
          }
          return profile;
        })
        .catch(function(err) {
          return err;
        });
    }

    function mailruProfile(access_token, uid) {
      var sig = md5.createHash(uid + 'app_id=753415method=users.getInfosecure=0session_key=' + access_token + appConfig.providers.mailru.private_key);
      var url = 'http://www.appsmail.ru/platform/api?method=users.getInfo&secure=0&app_id=753415&session_key=' + access_token + '&sig=' + sig
      return getProfile(url)
        .then(function(response) {
          var profile = {};
          if (response.data) {
            profile.first_name = response.data[0].first_name;
            profile.last_name = response.data[0].last_name;
            profile.full_name = profile.first_name + ' ' + profile.last_name;
          }
          return profile;
        })
        .catch(function(err) {
          return err;
        });
    }

    function getProfile(url) {
      return $http({
        method: 'GET',
        url: url
      });
    }

    function profileErr(err) {
      var resDefer = $q.defer();
      resDefer.resolve(err);
      return resDefer.promise;
    }
  }

})(angular);
