(function(angular) {
  'use strict';

  angular.module('app.main', ['angular-md5'])
    .config(appConfig);

  appConfig.$inject = ['$stateProvider'];
  function appConfig($stateProvider) {
    $stateProvider
      .state('main', {
        url: '/',
        controller: mainController,
        controllerAs: 'main',
        templateUrl: 'modules/main/main.html',
        resolve: {
          profile: profileResolve
        }
      });
  }

  profileResolve.$inject = ['userProfile'];
  function profileResolve(userProfile) {
    return userProfile.getData();
  }

  mainController.$inject = ['$state', 'profile'];
  function mainController($state, profile) {
    if (!profile) return $state.go('login');

    var vm = this;
    vm.profile = profile;
    vm.logout = logout;

    function logout() {
      $state.go('login');
    }
  }

})(angular);
