(function(angular) {
  'use strict';

  angular.module('app.auth', [])
    .config(authConfig);

  authConfig.$inject = ['$stateProvider'];
  function authConfig($stateProvider) {
    $stateProvider
      .state('login', {
        url: '/login',
        templateUrl: 'modules/auth/login.html',
        controller: 'LoginController',
        controllerAs: 'login',
        onEnter: onLoginEnter
      });
  }

  onLoginEnter.$inject = ['$ionicHistory'];
  function onLoginEnter($ionicHistory) {
    $ionicHistory.clearCache();
    $ionicHistory.clearHistory();
    localStorage.clear();
  }

})(angular);
