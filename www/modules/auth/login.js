(function(angular) {
  angular.module('app.auth')
    .controller('LoginController', LoginController)
    .factory('Auth', Auth);

  LoginController.$inject = ['$state', '$ionicLoading', '$ionicPopup', 'Auth'];
  function LoginController($state, $ionicLoading, $ionicPopup, Auth) {
    var vm = this;
    vm.vk = vk;
    vm.mailru = mailru;

    function vk() {
      $ionicLoading.show();
      localStorage.setItem('provider', 'vk');

      Auth.vk()
        .then(function(authParams) {
          localStorage.setItem('access_token', authParams.access_token);
          $ionicLoading.hide();
          return $state.go('main');
        })
        .catch(function(err) {
          return loginErr(err);
        });
    }

    function mailru() {
      $ionicLoading.show();
      localStorage.setItem('provider', 'mailru');

      Auth.mailru()
        .then(function(authParams) {
          localStorage.setItem('access_token', authParams.access_token);
          localStorage.setItem('uid', authParams.uid);
          $ionicLoading.hide();
          return $state.go('main');
        })
        .catch(function(err) {
          return loginErr(err);
        });
    }

    function loginErr(err) {
      $ionicLoading.hide();
      $ionicPopup.alert({
        templateUrl: 'modules/auth/noResponsePopup.html',
        cssClass: 'no-response-popup'
      });
    }
  }

  Auth.$inject = ['$q', '$window', 'appConfig'];
  function Auth($q, $window, appConfig) {
    return {
      vk: function() {
        return oAuth('vk')
          .then(function(data) {
            return {
              access_token: data.access_token
            };
          })
          .catch(function(err) {
            return err;
          });
      },
      mailru: function() {
        return oAuth('mailru')
          .then(function(data) {
            return {
              access_token: data.access_token,
              uid: data.x_mailru_vid
            };
          })
          .catch(function(err) {
            return err;
          });
      }
    };

    function oAuth(provider) {
      var resDefer = $q.defer();

      var authURL = appConfig.providers[provider].authURL;
      var browserRef = $window.open(encodeURI(authURL), '_blank', 'location=no');

      browserRef.addEventListener('loadstart', onLoadStart);
      browserRef.addEventListener('loadstop', onLoadStop);
      browserRef.addEventListener('exit', onExit);

      return resDefer.promise;

      function onLoadStart(event) {
        var tmp = (event.url).split('#');

        if (tmp[0] == appConfig.providers[provider].redirect_uri) {
          browserRef.removeEventListener('exit', function(event) {});
          browserRef.close();

          var callbackResponse = (event.url).split('#')[1];
          var responseParameters = (callbackResponse).split('&');
          var parameterMap = [];

          for (var i = 0; i < responseParameters.length; i++) {
            parameterMap[responseParameters[i].split('=')[0]] = responseParameters[i].split('=')[1];
          }

          if (parameterMap.access_token !== undefined && parameterMap.access_token !== null) {
            resDefer.resolve(parameterMap);
          } else {
            resDefer.reject('Problem authenticating');
          }
        }
      }

      function onLoadStop(event) {
        var tmp = (event.url).split('#');
        if (tmp[0] == appConfig.providers[provider].redirect_uri) {
          browserRef.close();
          resDefer.reject(tmp[1]);
        }
      }

      function onExit(event) {
        resDefer.reject('The sign in flow was canceled');
      }
    }
  }

})(angular);
